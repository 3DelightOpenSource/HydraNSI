#include "osoParserPlugin.h"

#include <3Delight/ShaderQuery.h>
#include <nsi_dynamic.hpp>

#include <pxr/base/gf/matrix4d.h>
#include <pxr/base/gf/vec3f.h>
#include <pxr/base/tf/staticTokens.h>
#include <pxr/base/vt/array.h>
#include <pxr/usd/sdr/shaderNode.h>
#include <pxr/usd/sdr/shaderProperty.h>
#include <pxr/usd/ndr/nodeDiscoveryResult.h>

PXR_NAMESPACE_OPEN_SCOPE

NDR_REGISTER_PARSER_PLUGIN(HdNSIOsoParserPlugin)

TF_DEFINE_PRIVATE_TOKENS(
    _tokens,

    ((discoveryType, "oso"))
    ((sourceType, "OSL"))
);

namespace
{
DlShaderInfo* GetShaderInfo(const char *i_path)
{
	static NSI::DynamicAPI api;
	static auto get_shader_info = api
		.LoadFunction<decltype(&DlGetShaderInfo)>("DlGetShaderInfo");
	if( !get_shader_info )
		return nullptr;
	return get_shader_info(i_path);
}

const TfToken& ShaderTypeToSdrType(const DlShaderInfo::Parameter &i_param)
{
	switch( i_param.type.elementtype )
	{
		case NSITypeFloat: return SdrPropertyTypes->Float;
		case NSITypeInteger: return SdrPropertyTypes->Int;
		case NSITypeString: return SdrPropertyTypes->String;
		case NSITypeColor: return SdrPropertyTypes->Color;
		case NSITypePoint: return SdrPropertyTypes->Point;
		case NSITypeVector: return SdrPropertyTypes->Vector;
		case NSITypeNormal: return SdrPropertyTypes->Normal;
		case NSITypeMatrix: return SdrPropertyTypes->Matrix;
		default: return SdrPropertyTypes->Unknown;
	}
}

VtValue ShaderValueToVtValue(const DlShaderInfo::Parameter &p)
{
	switch( p.type.elementtype )
	{
		case NSITypeFloat:
			if( p.fdefault.size() == 1 )
			{
				return VtValue{float(p.fdefault[0])};
			}
			else
			{
				return VtValue{
					VtArray<float>{p.fdefault.begin(), p.fdefault.end()}};
			}
		case NSITypeInteger:
			if( p.idefault.size() == 1 )
			{
				return VtValue{int(p.idefault[0])};
			}
			else
			{
				return VtValue{
					VtArray<int>{p.idefault.begin(), p.idefault.end()}};
			}
		case NSITypeString:
			if( p.sdefault.size() == 1 )
			{
				return VtValue{p.sdefault[0].string()};
			}
			else
			{
				VtArray<std::string> array;
				array.reserve(p.sdefault.size());
				for( const auto &v : p.sdefault )
					array.emplace_back(v.string());
				return VtValue{array};
			}
		case NSITypeColor:
		case NSITypePoint:
		case NSITypeVector:
		case NSITypeNormal:
			if( p.fdefault.size() == 3 )
			{
				return VtValue{GfVec3f(p.fdefault.data())};
			}
			else
			{
				VtArray<GfVec3f> array;
				size_t n = p.fdefault.size() / 3;
				array.reserve(n);
				for( size_t i = 0; i < n; ++i )
					array.emplace_back(p.fdefault.data() + i * 3);
				return VtValue{array};
			}
		case NSITypeMatrix:
			{
				GfMatrix4d m;
				for( unsigned i = 0; i < 16; ++i )
					m.data()[i] = p.fdefault[i];
				return VtValue{m};
			}
		default:
			return {};
	}
}

NdrPropertyUniquePtrVec BuildShaderProperties(DlShaderInfo *i_info)
{
	NdrPropertyUniquePtrVec properties;
	if( !i_info )
		return properties;

	for( const DlShaderInfo::Parameter &parameter : i_info->params() )
	{
		properties.emplace_back(
			new SdrShaderProperty(
				TfToken(parameter.name.c_str(), TfToken::Immortal),
				ShaderTypeToSdrType(parameter),
				ShaderValueToVtValue(parameter),
				parameter.isoutput,
				std::max(0, parameter.type.arraylen),
				{}, /* metadata */
				{}, /* hints */
				{})); /* options */
	}

	return properties;
}
}

NdrNodeUniquePtr HdNSIOsoParserPlugin::Parse(
    const NdrNodeDiscoveryResult& discoveryResult)
{
    DlShaderInfo *info = GetShaderInfo(discoveryResult.uri.c_str());

    return NdrNodeUniquePtr{new SdrShaderNode(
        discoveryResult.identifier,
        discoveryResult.version,
        discoveryResult.name,
        discoveryResult.family,
        _tokens->sourceType, /* Should probably be surface/displacement/etc */
        _tokens->sourceType,
        discoveryResult.uri,
#if !defined(PXR_VERSION) || PXR_VERSION > 1911
        discoveryResult.resolvedUri,
#endif
        BuildShaderProperties(info))};
}

const NdrTokenVec& HdNSIOsoParserPlugin::GetDiscoveryTypes() const
{
    static const NdrTokenVec discovery_types{_tokens->discoveryType};
    return discovery_types;
}

const TfToken& HdNSIOsoParserPlugin::GetSourceType() const
{
    return _tokens->sourceType;
}

PXR_NAMESPACE_CLOSE_SCOPE
// vim: set softtabstop=0 noexpandtab shiftwidth=4:
