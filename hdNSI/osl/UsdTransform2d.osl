shader UsdTransform2d(
    float in[2] = {0.0, 0.0},           // Input data to be transformed
    float rotation = 0.0,               // Counter-clockwise rotation in degrees
    float scale[2] = {1.0, 1.0},        // Scale factors for x and y
    float translation[2] = {0.0, 0.0},  // Translation to be applied
    output float result[2] = {0.0, 0.0} // Output transformed values
)
{
    // Convert rotation from degrees to radians    
    float radians_rotation = radians(rotation);

    // Calculate the sine and cosine of the rotation angle
    float cos_theta = cos(radians_rotation);
    float sin_theta = sin(radians_rotation);
    
    // Apply scale
    float scaled[2];
    scaled[0] = in[0] * scale[0];
    scaled[1] = in[1] * scale[1];
    
    // Apply rotation
    float rotated[2];
    rotated[0] = scaled[0] * cos_theta - scaled[1] * sin_theta;
    rotated[1] = scaled[0] * sin_theta + scaled[1] * cos_theta;

    // Apply translation
    result[0] = rotated[0] + translation[0];
    result[1] = rotated[1] + translation[1];
}